<!-- PROJECT SHIELDS -->
[![Latest Release](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/badges/release.svg)](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/releases) [![pipeline status](https://gitlab.com/cody-bolling/cicd/base-pipeline/badges/main/pipeline.svg)](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/commits/main)

<!-- PROJECT LOGO -->
<br/>
<div align="center">
  <a href="https://gitlab.com/cody-bolling/cicd/base-pipeline">
    <img src="https://gitlab.com/cody-bolling/cicd/project-boilerplate-generator/-/raw/main/logos/base-pipeline.png" alt="Logo" width="100" height="100">
  </a>
  <h3 align="center">
    Base Pipeline
  </h3>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary><b>Table of Contents</b></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#pipelines">Pipelines</a></li>
        <li><a href="#jobs">Jobs</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## About The Project

A collection of GitLab CICD jobs and pipelines intended to be used by other projects.

### Built With

- [AWS DynamoDB](https://aws.amazon.com/dynamodb/)
- [AWS ECR](https://aws.amazon.com/ecr/)
- [GitLab CICD](https://docs.gitlab.com/ee/ci/)

## Getting Started

### Pipelines

This is a list of pre-made pipelines to be used by other GitLab CICD projects. Each pipeline has the `CI/CD configuration file` that links to itself and a possible list of requirements.

#### How To Use

To use a pipeline, update the CI/CD configuration file in the project's settings.

1. Go to `Setting > CI/CD > General pipelines`.
2. Set the `CI/CD configuration file` to use the desired pipeline file.
3. Select the blue `Save changes` button.

Required variables can be passed in to the pipelines by having a `project-info.yaml` file at the root of the project. Below is an example of what the file should look like.

```yaml
variables:
  VERSION: 0.1.0
  TAGS: "GitLab CICD,AWS DynamoDB"
```

<details>
  <summary><b>Build Image Pipeline</b></summary>

  ---

  Pipeline that builds an image to a given destination and recreates a "latest" tag for the image. This pipeline also shares the same release functionality as the Release Pipeline.

  #### CI/CD Configuration File

  ```text
  pipelines/build-image-pipeline.yaml@cody-bolling/cicd/base-pipeline:main
  ```

  #### Required Variables

  - VERSION: The current version of the project.
  - TAGS: A list of strings that represent the tools and technologies used to build the project.
  - IMAGE_REGISTRY: The registry where the image will be hosted.
  - IMAGE_REPOSITORY: The repository within the registry that the image will be in.
  - IMAGE_NAME: The name of the image.

  #### Optional Variables

  - BUILD_CONTEXT: Build context of the project (default: $CI_PROJECT_DIR)
  - DOCKERFILE: Path to the Dockerfile (default: Dockerfile)
  - BUILD_ARGS: Build arguments to be passed to the Dockerfile (default: empty)

  [File Link](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/blob/main/pipelines/build-image-pipeline.yaml)

  ---

</details>

<details>
  <summary><b>Release Pipeline</b></summary>

  ---

  Pipeline that creates a new tag and release for a repository and sends the project's info to a DynamoDB table when a new merge to the main branch is made.

  #### CI/CD Configuration File

  ```text
  pipelines/release-pipeline.yaml@cody-bolling/cicd/base-pipeline:main
  ```

  #### Required Variables

  - VERSION: The current version of the project.
  - TAGS: A list of strings that represent the tools and technologies used to build the project.

  [File Link](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/blob/main/pipelines/release-pipeline.yaml)

  ---

</details>

### Jobs

This is a list of reusable GitLab CICD pipeline jobs in this repo. Each job has a How To Use section and a possible list of requirements.

<details>
  <summary><b>Build Image Job</b></summary>

  ---

  Builds and image using Kaniko into a given destination. Currently only supports AWS ECR. On non-main branches, the --no-push option will be set to true ensuring the build is just a test.

  #### How To Use

  ```yaml
  include:
    - project: cody-bolling/cicd/base-pipeline
      ref: main
      file: jobs/build.yaml

  Build Image:
    stage: build
    extends: .build
    variables:
      IMAGE_REGISTRY: my-registry
      IMAGE_REPOSITORY: my-repo
      IMAGE_NAME: image
      IMAGE_TAG: 1.0.0
  ```

  #### Required Variables

  - IMAGE_REGISTRY: The registry where the image will be hosted.
  - IMAGE_REPOSITORY: The repository within the registry that the image will be in.
  - IMAGE_NAME: The name of the image.
  - IMAGE_TAG: The tag or version of the image.

  #### Optional Variables

  - BUILD_CONTEXT: Build context of the project (default: $CI_PROJECT_DIR)
  - DOCKERFILE: Path to the Dockerfile (default: Dockerfile)
  - BUILD_ARGS: Build arguments to be passed to the Dockerfile (default: empty)

  [File Link](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/blob/main/jobs/build-image.yaml)

  ---

</details>

<details>
  <summary><b>Delete Image Tag Job</b></summary>

  ---

  Deletes the given tag of an image. Currently only supports AWS ECR.

  #### How To Use

  ```yaml
  include:
    - project: cody-bolling/cicd/base-pipeline
      ref: main
      file: jobs/delete-tag.yaml

  Delete Image Tag:
    stage: delete-tag
    extends: .delete-tag
    variables:
      IMAGE_REPOSITORY: my-repo
      IMAGE_NAME: image
      IMAGE_TAG: 1.0.0
  ```

  #### Required Variables

  - IMAGE_REPOSITORY: The repository within the registry that the image will be in.
  - IMAGE_NAME: The name of the image.
  - IMAGE_TAG: The tag or version of the image.

  [File Link](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/blob/main/jobs/delete-tag.yaml)

  ---

</details>

<details>
  <summary><b>Release Job</b></summary>

  ---

  When a merge to the main or master branch happens, this generates a new tag and release for a GitLab repository and inserts the project's information into the AWS DynamoDB table "git-repos". The information saved into AWS is the slug, title, url, latest commit date, project version, and project tags.

  #### How To Use

  ```yaml
  include:
    - project: cody-bolling/cicd/base-pipeline
      ref: main
      file: jobs/release.yaml

  Release:
    stage: release
    extends: .release
    variables:
      VERSION: 0.1.0
      TAGS: "GitLab CICD,Vue JS,AWS DynamoDB"
  ```

  #### Required Variables

  - VERSION: The current version of the project.
  - TAGS: A list of strings that represent the tools and technologies used to build the project.

  [File Link](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/blob/main/jobs/release.yaml)

  ---

</details>

## Roadmap

See the [open issues](https://gitlab.com/cody-bolling/cicd/base-pipeline/-/boards) for a full list of proposed features and known issues.

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.
